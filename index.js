const postsPerPage = 5;
let currentPage = 1;


function loadAllPosts(page) {
    fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${postsPerPage}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Error while loading posts');   
            }
            return response.json();
        })
        .then(data => {
            const posts = data.map(post => `

                <div class='post'>
                <a href="post_detail.html?id=${post.id}" class="list-group-item list-group-item-action mb-1">
                    <h3>Title: ${post.title}</h3>
                    <div id='user' class="text-secondary">By: ${post.userId}</div>
                    <div id='body'>Description: ${post.body}</div>
                </a>
                <div class='d-flex justify-content-end'>
                <button type="button" data-post-id="${post.id}" class="likeButton btn btn-danger position-relative">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat-heart" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M2.965 12.695a1 1 0 0 0-.287-.801C1.618 10.83 1 9.468 1 8c0-3.192 3.004-6 7-6s7 2.808 7 6-3.004 6-7 6a8 8 0 0 1-2.088-.272 1 1 0 0 0-.711.074c-.387.196-1.24.57-2.634.893a11 11 0 0 0 .398-2m-.8 3.108.02-.004c1.83-.363 2.948-.842 3.468-1.105A9 9 0 0 0 8 15c4.418 0 8-3.134 8-7s-3.582-7-8-7-8 3.134-8 7c0 1.76.743 3.37 1.97 4.6a10.4 10.4 0 0 1-.524 2.318l-.003.011a11 11 0 0 1-.244.637c-.079.186.074.394.273.362a22 22 0 0 0 .693-.125M8 5.993c1.664-1.711 5.825 1.283 0 5.132-5.825-3.85-1.664-6.843 0-5.132"></path>
                    </svg>
                    <span class="likesCount position-absolute top-0 start-100 translate-middle badge rounded-pill bg-primary" id="likesCount_${post.id}">
                        0
                    <span class="visually-hidden">unread messages</span>
                    </span>
                </div>
                </div>
            `).join('');
            document.getElementById('content').innerHTML = posts;
            document.querySelectorAll('.likeButton').forEach(button => {
                button.addEventListener('click', function() {
                    const postId = button.getAttribute('data-post-id');
                    likePost(postId);
                });
            });
        })
        .catch(error => {
            console.error(error);
            alert('Error while loading posts');
        });
}

function likePost(postId) {
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
        method: 'PATCH',
        body: JSON.stringify({
            likes: 1 
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Error while liking post');
        }
        return response.json();
    })
    .then(updatedPost => {

        const likesCountElement = document.getElementById(`likesCount_${postId}`);
        likesCountElement.textContent = updatedPost.likes;
    })
    .catch(error => {
        console.error(error);
        alert('Error while liking post');
    });
}

function createNewPost() {
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: 'foo',
            body: 'bar',
            userId: 1,
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
        })
    .then(response => {
        if (!response.ok) {
            throw new Error('Ошибка при создании поста');
        }
        return response.json();
    })
    .then(data => {
        alert('Пост успешно создан!');
        window.location.href = 'index.html';
    })
    .catch(error => {
        console.error(error);
        alert('Ошибка при создании поста.');
    });
}



function loadPost(postId) {
    const title = document.getElementById('postTitle').value;
    const body = document.getElementById('postBody').value;
    const userId = document.getElementById('userId').value;

    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}/`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Error while loading posts');   
            }
            return response.json();
        })
        .then(post => {
            const postDetail = `
                <div class='post'>
                    <h3>Title: ${post.title}</h3>
                    <div id='user' class="text-secondary">By: ${post.userId}</div>
                    <div id='body'>Description: ${post.body}</div>
                </div>
                `;
            document.getElementById('contentDetail').innerHTML = postDetail;
        })
        .catch(error => {
            console.error(error);
            alert('Error while loading post detail');
        });
    }

const urlId = new URLSearchParams(window.location.search);
const postId = urlId.get('id');

function updatePost() {
    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        body: JSON.stringify({
        id: 1,
        title: 'foo',
        body: 'bar',
        userId: 1,
    }),
    headers: {
        'Content-type': 'application/json; charset=UTF-8',
    },
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Ошибка при создании поста');
        }
        return response.json();
    })
    .then(data => {
        alert('Пост успешно отредактирован!');
        loadPost(postId);
    })
    .catch(error => {
        console.error(error);
        alert('Ошибка при редактировании поста поста.');
    }); 
}

function deletePost() {
    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'DELETE',
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Ошибка при удалении поста');
        }
        return response.json();
    })
    .then(data => {
        alert('Пост успешно удален!');
        window.location.href = 'index.html';
    })
}    

function loadComments(postId) {
    fetch(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Error while loading comments');
            }
            return response.json();
        })
        .then(comments => {
            const commentsHTML = comments.map(comment => `
                <div class="comment">
                    <h6>Username: ${comment.name}</h6>
                    <p>Comment: ${comment.body}</p>
                </div>
            `).join('');
            document.getElementById('comments').innerHTML = commentsHTML;
        })
        .catch(error => {
            console.error(error);
            alert('Error while loading comments');
        });
}

document.addEventListener('DOMContentLoaded', function() {
    $('#navigation').load('navigation.html');
});


document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById('content')) {
        function calcTotalPages() {
            fetch('https://jsonplaceholder.typicode.com/posts')
                .then(response => {
                    return response.json()
                })
                .then(data =>{
                    totalPages = Math.ceil(data.length / postsPerPage);
                })
        }
        
        loadAllPosts(currentPage);
        calcTotalPages()

        document.getElementById('createPost').addEventListener('click', function(event) {
            event.preventDefault(); 
            document.getElementById('content').style.display = 'none'
            document.getElementById('paginationId').style.display = 'none'
            document.getElementById('createPostForm').style.display = 'block';
            });
        
        document.getElementById('cancelButton').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('content').style.display = 'block';
            document.getElementById('paginationId').style.display = 'block'
            document.getElementById('createPostForm').style.display = 'none';
            });

        document.getElementById('submitPostButton').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('content').style.display = 'block';
            document.getElementById('paginationId').style.display = 'block'
            document.getElementById('createPostForm').style.display = 'none';
            createNewPost();
            });

        document.addEventListener('click', function (event) {
            if (event.target.matches('#nextPageLink')) {
                event.preventDefault();
                if (currentPage < totalPages) {
                    currentPage++;
                    loadAllPosts(currentPage);
                }
            }
        });

        document.addEventListener('click', function (event) {
            if (event.target.matches('#prevPageLink')) {
                event.preventDefault();
                if (currentPage > 1) {
                    currentPage--;
                    loadAllPosts(currentPage);
                }
            }
        });

        document.addEventListener('click', function (event) {
            if (event.target.matches('#firstPageLink')) {
                event.preventDefault();
                currentPage = 1;
                loadAllPosts(currentPage);
            }
        });

        document.addEventListener('click', function (event) {
            if (event.target.matches('#lastPageLink')) {
                event.preventDefault();
                currentPage = totalPages;
                loadAllPosts(currentPage);
            }

        });
    } else if (document.getElementById('contentDetail')) {
        if (postId) {
            loadPost(postId);
        } else {
            alert('Post id is not found')
        }
        
        document.getElementById('createPost').style.display = 'none'

        document.getElementById('updatePost').addEventListener('click', function(event) {
            event.preventDefault(); 
            document.getElementById('contentDetail').style.display = 'none'
            document.getElementById('updatePost').style.display = 'none'
            document.getElementById('deletePost').style.display = 'none'
            document.getElementById('updatePostForm').style.display = 'block';
            fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Error while loading post data');
                    }
                    return response.json();
                })
                .then(post => {
                    document.getElementById('postTitle').value = post.title;
                    document.getElementById('postBody').value = post.body;
                    document.getElementById('userId').value = post.userId;
                })
                .catch(error => {
                    console.error(error);
                    alert('Error while loading post data');
                });
            });   

        document.getElementById('submitUpdatePostButton').addEventListener('click', function(event) {
            event.preventDefault(); 
            document.getElementById('contentDetail').style.display = 'block'
            document.getElementById('updatePost').style.display = 'block'
            document.getElementById('deletePost').style.display = 'block'
            document.getElementById('updatePostForm').style.display = 'None';
            updatePost();
        });     

        document.getElementById('deletePost').addEventListener('click', function(event) {
            event.preventDefault(); 
            deletePost();
        });

        document.getElementById('commentAddForm').addEventListener('submit', function(event) {
            event.preventDefault();
        
            const userName = document.getElementById('userName').value;
            const commentBody = document.getElementById('commentBody').value;
            if (postId) {
                loadPost(postId);
            } else {
                alert('Post id is not found')
            }

            fetch(`https://jsonplaceholder.typicode.com/comments`, {
                method: 'POST',
                body: JSON.stringify({
                    postId: postId, 
                    name: userName,
                    body: commentBody,
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Error while adding a new comment');
                }
                return response.json();
            })
            .then(newComment => {
                alert('Comment successfully added!');
                loadComments(postId);
            })
            .catch(error => {
                console.error(error);
                alert('Error while adding a new comment');
            });
        });        
    } 
});

